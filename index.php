<?php
session_start();
session_regenerate_id();

include 'main_header.php';

if(!isset($_SESSION['uname'])) {
  if(isset($_POST['btn_register'])){
    include_once 'register.php';
  }else{
    include_once 'login.php';
  }
}else{
  include_once 'search_bar.php';
}

include_once 'main_footer.php';
