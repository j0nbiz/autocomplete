<?php

include_once 'database_utils.php';

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    if(isset($_GET['in_search']) & !empty($_GET['in_search'])){
        if(!is_in_history($_GET['in_search'])){
            update_history($_GET['in_search']);
        }else{
            update_term_date($_GET['in_search']);
        }
    }
}

$history = get_history(); // Define here to not query database too much

echo '
<div class="main">
    <div class="search">
        <form id="search_bar" method="GET" action="">
            <input class="search" id="in_search" name="in_search" type="text" autocomplete="off">
        </form>
        <div id="autocomplete"></div>

        <div class="history">
          <p class="label">Search History</p>
          <p class="history"><span class="date">'.$history[0][1].'</span>&#160&#160&#160'.$history[0][0].'</p>
          <p class="history"><span class="date">'.$history[1][1].'</span>&#160&#160&#160'.$history[1][0].'</p>
          <p class="history"><span class="date">'.$history[2][1].'</span>&#160&#160&#160'.$history[2][0].'</p>
          <p class="history"><span class="date">'.$history[3][1].'</span>&#160&#160&#160'.$history[3][0].'</p>
          <p class="history"><span class="date">'.$history[4][1].'</span>&#160&#160&#160'.$history[4][0].'</p>
        </div>
        <div class="btn_set">
            <form method="POST" action="">
                <button name="btn_logout" value="btn_logout" type="submit">Logout</button>
                <button form="search_bar" name="btn_submit" value="btn_submit" type="submit">Submit</button>
            </form>
        <div>
    </div>
</div>';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['btn_logout'])){
      echo 'logout';
        session_destroy();
        unset($_SESSION);
        header('Location: index.php'); // Redirect to the index
    }
}
