<?php
include_once 'database_utils.php';

echo '
<div class="main">
    <div class="login">
        <form method="POST" action="">
            <p class="label">Username</p>
            <input class="forms" name="in_uname" type="text" autocomplete="off">
            <p class="label">Password</p>
            <input class="forms" name="in_pword" type="password" autocomplete="off">';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['btn_login'])){
        if(isset($_POST['in_uname']) & isset($_POST['in_pword'])){
            if(!empty($_POST['in_uname']) & !empty($_POST['in_pword'])){
                if(!is_locked_out($_POST['in_uname'])){
                    if(verify($_POST['in_uname'], $_POST['in_pword'])){
                        session_start();
                        session_regenerate_id();
                        $_SESSION['uname'] = $_POST['in_uname'];
                        header('Location: index.php'); // Redirect to the index
                    }
                    else {
                        echo '<p class="warn">Incorect username or password combination!</p>';
                    }
                }else{
                    echo '<p class="warn">Too many login attempts on with this username!</p>';
                }
            }else{
                echo '<p class="warn">Fill in all the required fields!</p>';
            }
        }
    }
}

echo '
<div class="btn_set">
  <button name="btn_login" value="btn_login" type="submit">Login</button>
  <button name="btn_register" value="btn_register" type="submit">Register</button>
</div>
</form>
</div>
</div>';
