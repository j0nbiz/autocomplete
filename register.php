<?php
include_once 'database_utils.php';

echo '
<div class="main">
    <div class="register">
        <form method="POST" action="">
            <p class="label">Username</p>
            <input class="forms" name="in_uname" type="text" autocomplete="off">
            <p class="label">Password</p>
            <input class="forms" name="in_pword" type="password" autocomplete="off">
            <p class="label">Confirm Password</p>
            <input class="forms" name="in_pword_check" type="password" autocomplete="off">';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['btn_register'])){
        if(isset($_POST['in_uname']) & isset($_POST['in_pword']) & isset($_POST['in_pword_check'])){
            if(!empty($_POST['in_uname']) & !empty($_POST['in_pword']) & !empty($_POST['in_pword_check'])){
                if($_POST['in_pword'] === $_POST['in_pword_check']){
                    if(is_unique($_POST['in_uname'])){
                        create_user($_POST['in_uname'], $_POST['in_pword']);
                        session_start();
                        session_regenerate_id();
                        $_SESSION['uname'] = $_POST['in_uname'];
                        header('Location: index.php'); // Redirect to the index
                    }
                    else {
                        echo '<p class="warn">Username already in use!</p>';
                    }
                }else{
                  echo '<p class="warn">Passwords do not match!</p>';
                }
            }else{
                echo '<p class="warn">Fill in all the required fields!</p>';
            }
        }
    }elseif(isset($_POST['btn_return'])){
        header('Location: index.php'); // Redirect to the index
    }
}

echo '
<div class="btn_set">
  <button name="btn_register" value="btn_register" type="submit">Register</button>
  <button name="btn_return" value="btn_return" type="submit">Return</button>
</div>
</form>
</div>
</div>';
