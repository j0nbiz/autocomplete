$("document").ready(function(){
		var $main_left =  parseInt($(".main").css("marginLeft"));
		var $main_top = parseInt($(".main").css("marginTop"));

		$("#in_search").keyup(function(){
				var pattern = this.value;

				if(!$(this).val()){
					$("#autocomplete").text(""); // Reset div if not value
				}

				$.ajax({
					url: "json_utils.php",
					data: {in_search:pattern},
					type: "GET",
					dataType: "json",
					success: function(json){
						if(json.length != 0){
							$("#autocomplete").text(""); // Reset div
							for(var i = 0; i < json.length; i++){
								$("#autocomplete").append("<div id='entry" + i + "' onclick='setSearch(this)'><span class='bold'>" + json[i].substring(0, $("#in_search").val().length) + "</span>" + json[i].substring($("#in_search").val().length) + "</div>");
							}
						}else{
							$("#autocomplete").text(""); // Reset div
							$("#autocomplete").append("<span class='bold'>" + $("#in_search").val() + "</span>...");
						}
					}
				});
		});

		// Mouse tracking with damping, opacity  and size effect ( actualy by me <:]~ plz don't steal this Jaya :[ )
		// damp variables
		var $damp_x_1 = 0, $damp_y_1 = 0;
		var $damp_x_2 = 0, $damp_y_2 = 0;
		var $damp_x_3 = 0, $damp_y_3 = 0;

		// opacity variables
		var $dif_x_1 = 0, $dif_y_1 = 0;
		var $dif_x_2 = 0, $dif_y_2 = 0;
		var $dif_x_3 = 0, $dif_y_3 = 0;
		var $opacity_1 = 0, $opacity_2 = 0, $opacity_3 = 0;

		$(document).mousemove(function(e){
				$dif_x_1 = e.pageX - $("#by").position().left;
				$dif_y_1 = e.pageY - $("#by").position().top;
				$opacity_1 = Math.abs((($dif_x_1 + $dif_y_1) / 2) / 80);

				$dif_x_2 = e.pageX - $("#j0n").position().left;
				$dif_y_2 = e.pageY - $("#j0n").position().top;
				$opacity_2 = Math.abs((($dif_x_2 + $dif_y_2) / 2) / 80);

				$dif_x_3 = e.pageX - $("#biz").position().left;
				$dif_y_3 = e.pageY - $("#biz").position().top;
				$opacity_3 = Math.abs((($dif_x_3 + $dif_y_3) / 2) / 80);

				$('#by').css({
					 left: $damp_x_1 += ((e.pageX - $damp_x_1) / 60),
					 top: $damp_y_1 += ((e.pageY - $damp_y_1) / 60),
					 opacity: $opacity_1,
					 fontSize: $opacity_1 * 10
				});
				$('#j0n').css({
					 left: $damp_x_2 += ((e.pageX - $damp_x_2) / 80),
					 top: $damp_y_2 += ((e.pageY - $damp_y_2) / 80),
					 opacity: $opacity_2,
					 fontSize: $opacity_3 * 10
				});
				$('#biz').css({
				    left: $damp_x_3 += ((e.pageX - $damp_x_3) / 100),
				    top: $damp_y_3 += ((e.pageY - $damp_y_3) / 100),
					 opacity: $opacity_3,
					 fontSize: $opacity_3 * 10
				});

				// Move menu for fun
					if((e.pageX > ($(window).width() / 2) - 400) & (e.pageX < ($(window).width() / 2) - 270)){
						if((e.pageY > 80) & (e.pageY < $(".main").height() + 120)){
							$(".main").css({
								marginLeft: 	$main_left + 40
							});
						}else{
							$(".main").css({
								marginLeft: 	$main_left
							});
						}
					}else if ((e.pageX > ($(window).width() / 2) + 270) & (e.pageX < ($(window).width() / 2) + 400)) {
						if((e.pageY > 80) & (e.pageY < $(".main").height() + 120)){
							$(".main").css({
								marginLeft: 	$main_left - 40
							});
						}else{
							$(".main").css({
								marginLeft: 	$main_left
							});
						}
					}else if((e.pageX > ($(window).width() / 2) - 250) & (e.pageX < ($(window).width() / 2) + 250)) {
						if((e.pageY > $(".main").height() + 140) & (e.pageY < $(".main").height() + 200)){
													console.log("zone");
							$(".main").css({
								marginTop: 	$main_top - 40
							});
						}else{
							$(".main").css({
								marginTop: 	$main_top
							});
						}
					}else{
						$(".main").css({
							marginLeft: $main_left,
							marginTop: 	$main_top
						});
					}
		});
});

function init(){
		var in_search = document.getElementById("in_search");
}

function setSearch(e){
		document.getElementById("search_bar").submit;
		in_search.value = e.textContent; // Set div content to searchbar
}

window.onload = init;
