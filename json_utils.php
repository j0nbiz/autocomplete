<?php
session_start();
session_regenerate_id();

require_once 'database_utils.php';

header('content-type:application/json');

// Only return json if something is in the search field
if(isset($_GET['in_search']) & !empty($_GET['in_search'])){
  $pattern = $_GET['in_search'];
  $cities = get_cities_matching($pattern);
}

echo json_encode($cities);
